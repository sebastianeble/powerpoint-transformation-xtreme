# PowerPoint Transformation Xtreme (PPTX)

This repository contains our pipeline for the automatic conversion of a set of slides into a video with spoken text and an lip-synced speaker. This pipeline consists of the following steps in their respective directories.

- `avatar-generation` contains sources and a Dockerfile for the avatar-generation step

- `create-video` contains sources and a Dockerfile for the vide-creation step

- `lip-sync` contains sources and a Dockerfile for the lip-syncing step

- `pipeline` 

- `text-to-text` contains sources and a Dockerfile for the text-to-text step

- `voice-cloning` contains sources and a Dockerfile for the text-to-speech and voice-cloning steps



If you want to recreate the pipeline yourself, look into the `README.md` in the pipeline directory


