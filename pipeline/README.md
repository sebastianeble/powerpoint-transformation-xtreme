# PowerPoint Transformation Xtreme (PPTX)

## Pipeline

### Prerequisites

- recent Docker Desktop Installation

- Python installed

- Sufficiently powered Nvidia GPU. This pipeline run successfully on two computers with Intel CPU and Nvidia GPU. Other computers with AMD GPUs or Apples M1/2 processor had issues. There may still be some unforseen issues, if you encounter any, please report them
  
  

### Instructions

1. Open a shell in this directory

2. Optional: If you don't want your global python environment polluted
   
   1. Create a venv
   
   2. Activate the new venv

3. `pip install -r requirements.txt`

4. `python main.py`



### Troubleshooting

- Docker containers can't be built or don't run successfully:
  
  - In our experience this was the case on machines without a Nvidia GPU

- There is no input validation for the input parameters in the GUI, so please check everything you put in

- If the containers crash, it may help to explicitely build them with a command:
  
  - Close the application
  
  - `docker compose -f compose.yaml build`
  
  - Open again

- If the containers crash, they may still be in a running state (which can cause issues in the next run) and have to be manually removed with
  
  - `docker compose -f compose.yaml rm -f -s -v`
  
  - This command is run by the UI, but won't be if there's a previous error

- Really long sets of slides may run into resource issues during video creation. But pipeline was tested with 10 slides
